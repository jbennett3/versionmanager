<!doctype html>
<html lang="en">

<head>
  <script src="../../_layouts/15/MicrosoftAjax.js"></script>
  <script src="../../_layouts/15/init.js"></script>
  <script src="../../_layouts/15/core.js"></script>
  <script src="../../_layouts/15/sp.core.js"></script>
  <script src="../../_layouts/15/sp.init.js"></script>
  <script src="../../_layouts/15/sp.runtime.js"></script>
  <script src="../../_layouts/15/sp.js"></script>
  <script src="../../_layouts/15/sp.RequestExecutor.js"></script>
  <script src="version-manager.js"></script>
</head>

<body>
  <script>
    VersionManager([
      "app.bundle",
      "vendor.bundle",
    ]);
  </script>
</body>

</html>