
function VersionManager(BUNDLES) {
  function parseUrl() {
    var url = window.location.href;

    var currUrl = url;
    var indices = [];
    var index = 0;
    var nextIndex;
    while ((nextIndex = currUrl.indexOf("/")) !== -1) {
      index += nextIndex + 1;
      indices.push(index - 1);
      currUrl = currUrl.substr(nextIndex + 1);
    }
    var sitesIndex = indices[2];
    var siteIndex = indices[indices.length - 2];
    var docLibIndex = indices[indices.length - 1];

    return {
      sitePath: url.substr(sitesIndex, siteIndex - sitesIndex) + "/",
      docLibPath: url.substr(sitesIndex, docLibIndex - sitesIndex) + "/",
      docLib: url.substr(siteIndex + 1, docLibIndex - siteIndex - 1),
    }
  }
  var parsedUrl = parseUrl();
  var sitePath = parsedUrl.sitePath;
  var docLibPath = parsedUrl.docLibPath;
  var docLib = parsedUrl.docLib;

  function htmlTemplate(indexStr, bundleName, versionStr) {
    var replace = bundleName + "\..*?\.?js";
    var re = new RegExp(replace, "g");
    return indexStr.replace(re, bundleName + "." + versionStr + ".js");
  }


  function writeIndex(contents, success, error) {
    var executor = new SP.RequestExecutor(sitePath);
    executor.executeAsync(
      {
        url: sitePath + '_api/Web/getfolderbyserverrelativeurl(\'' + docLib + '\')/files/add(overwrite=true,url=\'index.aspx\')',
        method: "POST",
        body: contents,
        success: function () {
          success();
        },
        error: function (response) {
          error(response.statusText);
        }
      }
    );
  }

  function readIndex(success, error) {
    var executor = new SP.RequestExecutor(sitePath);
    executor.executeAsync({
      url: docLibPath + "index.aspx",
      method: "GET",
      success: function (response) {
        success(response.body);
      },
      error: function (response) {
        error(response.statusText);
      }
    }
    );
  }

  function getVersions(bundleName, then, failed) {
    var context = new SP.ClientContext(sitePath);
    var appLib = context.get_web().get_lists().getByTitle(docLib);
    var query = new SP.CamlQuery();
    query.set_viewXml
      ('<View Scope="RecursiveAll">'
      + '  <ViewFields>'
      + '    <FieldRef Name="FileLeafRef"/>'
      + '    <FieldRef Name="Modified"/>'
      + '  </ViewFields> '
      + '  <Query>'
      + '    <Where>'
      + '      <Contains>'
      + '        <FieldRef Name="FileLeafRef"/>'
      + '        <Value Type="Text">' + bundleName + '</Value>'
      + '      </Contains>'
      + '    </Where>'
      + '  </Query>'
      + '</View>'
      );
    var items = appLib.getItems(query);
    context.load(items);

    var versions = [];
    context.executeQueryAsync(
      function () {
        var enumerator = items.getEnumerator();
        while (enumerator.moveNext()) {
          let fileName = enumerator.get_current().get_item("FileLeafRef");
          if (fileName.indexOf("map") === -1) {
            let modifiedDate = enumerator.get_current().get_item("Modified");
            var version =
              fileName
                .replace(bundleName + ".", "")
                .replace(".js", "")
              ;
            if (version !== "js")//the case where no version label was specified on the file name
            {
              versions.push([version, modifiedDate]);
            }
          }
        }
        then(versions);
      }
      , function (sender, err) {
        failed(err.get_message());
      }
    );
  }

  ////////////////////// UI //////////////////////////////
  var DATE_DELIMITER = " -- "
  var table = document.createElement("table")
  var tbody = document.createElement("tbody")
  table.appendChild(tbody);
  document.body.appendChild(table);

  var elements = {};
  function buildRow(bundleName) {
    var tr = document.createElement("tr");
    tbody.appendChild(tr);

    var row = {
      bundle: document.createElement("div"),
      selector: document.createElement("select"),
      button: document.createElement("button"),
      message: document.createElement("div"),
    };

    for (var key in row) {
      var td = document.createElement("td");
      td.appendChild(row[key]);
      tr.appendChild(td);
    }
    row.bundle.innerHTML = bundleName;
    row.button.innerHTML = "Set Version"
    row.button.onclick = function () {
      readIndex(function (indexStr) {
        writeIndex(
          htmlTemplate(indexStr, bundleName, row.selector.value.split(DATE_DELIMITER)[0].trim()),
          function () {
            row.message.style.color = "green";
            row.message.innerHTML = "Success!";
          }
          , function (errorMessage) {
            row.message.style.color = "red";
            row.message.innerHTML = errorMessage;
          }
        );
      },
        function (errorMessage) {
          messageLabel.style.color = "red";
          messageLabel.innerHTML = errorMessage;
        }
      );
    }
    row.selector.onchange = function () {
      row.message.innerHTML = "";
    }
    elements[bundleName] = row;
  }

  BUNDLES.forEach(function (bundleName) {
    buildRow(bundleName);
    getVersions(
      bundleName,
      function (versions) {
        versions.forEach(function (version, i) {
          var option = document.createElement("option");
          option.innerHTML = version[0] + DATE_DELIMITER + version[1];
          elements[bundleName].selector.appendChild(option);
        });
      },
      function (errorMessage) {
        elements[bundleName].message.style.color = "red";
        elements[bundleName].message.innerHTML = errorMessage;
      });
  });
}